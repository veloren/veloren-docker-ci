FROM ubuntu:22.04 as fetch

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends --assume-yes \
        time \
        # ~12 mb
        curl \
        # ~2.8 mb
        ca-certificates;

# Download and extract vulkan SDK
# NOTE: newer versions are in .dmg files which I can't get open
RUN time curl --proto '=https' --tlsv1.2 -sSf https://sdk.lunarg.com/sdk/download/1.3.216.0/mac/vulkan_sdk.tar.gz | tar -xz

RUN mkdir -p /shaderc/combined \
    && mv vulkansdk-macos-1.3.216.0/macOS/lib/libshaderc_combined.a /shaderc/combined

FROM scratch
COPY --from=fetch /shaderc /shaderc

# NOTE: the resulting shaderc lib here doesn't actually work for linking (see note in cache/macos/Dockerfile)
