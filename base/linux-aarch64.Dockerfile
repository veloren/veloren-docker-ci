# Serves as base for linux release build, cargo check, cargo test, cargo bench
ARG FROM_TAG=please-specify-a-tag-also-hopefully-this-tag-wont-be-used
# COPY --from doesn't support ARG expansion so we need to first use FROM to
# create a stage to copy from, see relevant section in:
# https://medium.com/@tonistiigi/advanced-multi-stage-build-patterns-6f741b852fae
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/common:$FROM_TAG


# Make dpkg / apt changes necessary for cross-compilation
RUN dpkg --add-architecture arm64 \
    && echo "deb [arch=amd64,i386] http://archive.ubuntu.com/ubuntu/ jammy main restricted universe multiverse\n\
deb [arch=amd64,i386] http://archive.ubuntu.com/ubuntu/ jammy-updates main restricted universe multiverse\n\
deb [arch=amd64,i386] http://archive.ubuntu.com/ubuntu/ jammy-backports main restricted universe multiverse\n\
deb [arch=amd64,i386] http://security.ubuntu.com/ubuntu jammy-security main restricted universe multiverse\n\
deb [arch=arm64,armhf,ppc64el,s390x] http://ports.ubuntu.com/ubuntu-ports/ jammy main restricted universe multiverse\n\
deb [arch=arm64,armhf,ppc64el,s390x] http://ports.ubuntu.com/ubuntu-ports/ jammy-updates main restricted universe multiverse\n\
deb [arch=arm64,armhf,ppc64el,s390x] http://ports.ubuntu.com/ubuntu-ports/ jammy-backports main restricted universe multiverse\n\
deb [arch=arm64,armhf,ppc64el,s390x] http://ports.ubuntu.com/ubuntu-ports/ jammy-security main restricted universe multiverse\n"\
    > /etc/apt/sources.list

# Install neccesary packages for linux compile
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && time apt-get install -y --no-install-recommends --assume-yes \
        # ~89 mb (NOTE: needed for serde as linker) (NOTE: duplicated with base/windows and base/linux)
        gcc \
        # ~89 mb
        gcc-aarch64-linux-gnu \
        # ~25 mb (NOTE: duplicated with base/windows and base/linux)
        libc-dev \
        # ~25 mb
        libc6-dev-arm64-cross \
        # ~1 mb (required by alsa-sys build script)
        picolibc-aarch64-linux-gnu \
        # ~0.2 mb (required by alsa-sys build script)
        libudev-dev:arm64 \
        # ~2.6 mb (required by alsa-sys build script)
        libasound2-dev:arm64 \
        # Required by alsa-sys build script
        dpkg-dev \
        \
        # ~22.6 mb (required for xcb build script)
        python3 \
        # ~3.4 mb (required for xcb linking)
        libxcb-xkb-dev:arm64 \
        # ~4.4 mb (required for xkb linking)
        libxkbcommon-x11-dev:arm64 \
        # ~0.28 mb
        libxcb-render0-dev:arm64 \
        # ~0.1 mb
        libxcb-shape0-dev:arm64 \
        # ~0.18 mb
        libxcb-xfixes0-dev:arm64 \
        \
        # In addition for benchmarks and tests:
        # ~7.9 mb (for openssl-sys build script)
        libssl-dev:arm64 \
        # ~42 mb (for minifb build script)
        g++-aarch64-linux-gnu \
        # ~4 mb (for storing benchmark data)
        postgresql-client \
        # Required for iced
        libfontconfig1-dev:arm64 \
        # Used to zip the airship installer
        zip \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*

# Needed for building shaderc
# TODO: remove if we ever update shaderc to the version that supports python3
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# Install linux aarch64 toolchain
RUN echo \
        "[target.aarch64-unknown-linux-gnu]\nlinker=\"aarch64-linux-gnu-gcc\"\n" \
        >> /root/.cargo/config \
    && . /root/.cargo/env \
    && time rustup target add aarch64-unknown-linux-gnu
