# Serves as base for linux release build, cargo check, cargo test, cargo bench
ARG FROM_TAG=please-specify-a-tag-also-hopefully-this-tag-wont-be-used
# COPY --from doesn't support ARG expansion so we need to first use FROM to 
# create a stage to copy from, see relevant section in:
# https://medium.com/@tonistiigi/advanced-multi-stage-build-patterns-6f741b852fae 
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/shaderc-linux-x86_64:${FROM_TAG} as shaderc-linux-forcopy
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/common:$FROM_TAG

# Install neccesary packages for linux compile
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && time apt-get install -y --no-install-recommends --assume-yes \
        # ~89 mb (NOTE: duplicated with base/windows)
        gcc \
        # ~25 mb (NOTE: duplicated with base/windows)
        libc-dev \
        # ~9 mb (required by alsa-sys build script)
        pkg-config \
        # ~0.2 mb (required by alsa-sys build script)
        libudev-dev \
        # ~2.6 mb (required by alsa-sys build script)
        libasound2-dev \
        \
        # ~22.6 mb (required for xcb build script)
        python3 \
        # ~3.4 mb (required for xcb linking)
        libxcb-xkb-dev \
        # ~4.4 mb (required for xkb linking)
        libxkbcommon-x11-dev \
        # ~0.28 mb
        libxcb-render0-dev \
        # ~0.1 mb
        libxcb-shape0-dev \
        # ~0.18 mb
        libxcb-xfixes0-dev \
        \
        # In addition for benchmarks and tests:
        # ~7.9 mb (for openssl-sys build script)
        libssl-dev \
        # ~42 mb (for minifb build script)
        g++ \
        # ~4 mb (for storing benchmark data)
        postgresql-client \
        # Required for iced
        libfontconfig1-dev \
        # Used to zip the airship installer
        zip \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

# Copy shaderc
COPY --from=shaderc-linux-forcopy /shaderc/ /shaderc/
