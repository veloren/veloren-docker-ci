FROM registry.gitlab.com/veloren/veloren-docker-ci/base/common:$FROM_TAG

# We need a newer version of mingw to cross-compile >=libmimalloc-sys@v0.1.39
# for windows. Base is kept the same as in common.Dockerfile to not pull in
# another base, and said base is kept old (for now) to ensure linux users don't
# need an all-too new glibc version to run the game.
RUN echo \
        "deb http://archive.ubuntu.com/ubuntu/ noble main restricted universe multiverse \
        \n\ndeb http://archive.ubuntu.com/ubuntu/ noble-updates main restricted universe multiverse \
        \n\ndeb http://archive.ubuntu.com/ubuntu/ noble-security main restricted universe multiverse \
        \n\ndeb http://archive.ubuntu.com/ubuntu/ noble-backports main restricted universe multiverse" \
        >> /etc/apt/sources.list \
    # Install necessary packages for windows cross compile
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && time apt-get install -y --no-install-recommends --assume-yes \
        -t noble -t noble-updates -t noble-security -t noble-backports \
        # Needed to cross compile to windows
        gcc-mingw-w64-x86-64 \
        gcc \
        libc-dev \
        python3 \
        g++-mingw-w64-x86-64 \
        # Required to build NSIS Windows installers (used by Airshipper)
        nsis \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

# Install the NSIS EnVar plugin
COPY base/nsis_plugins/x86-unicode/EnVar.dll /usr/share/nsis/Plugins/x86-unicode/

# Install windows gnu toolchain
RUN echo \
        "[target.x86_64-pc-windows-gnu]\nlinker = \"/usr/bin/x86_64-w64-mingw32-gcc\"\n" \
        >> /root/.cargo/config.toml \
    && . /root/.cargo/env \
    && time rustup target add x86_64-pc-windows-gnu
