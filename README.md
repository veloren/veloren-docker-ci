# Veloren Docker CI

## Installing a Veloren CI Runner via docker

This holds the Dockerfile that is used to build the CI for Veloren. It also has a script that will set up a runner on a Linux machine. These are the requirements for a runner:

- 65gb free hard drive space
- At least 4 cores or VCPUs
- 8gb avaiable ram + 12 gb swap configured
- An unlimited internet connection, as downloads and uploads are done for each run

On top of these requirements, you must get approval from one of the admins of Veloren. This is because we don't want to give the runner token out to everyone, as that could pose a security risk. Ping @AngelOnFira or @xMAC94x for more information.

Once you have the token, clone the repo and run the launch script.

```
git clone https://gitlab.com/veloren/veloren-docker-ci.git
cd veloren-docker-ci/runner
./launch-runner.sh
```

## Make changes to the runner image:
```
git clone https://gitlab.com/veloren/veloren-docker-ci.git
cd veloren-docker-ci
./docker-build.sh
```

## Update to toolchain
If you want to update the toolchain and refresh the cache execute the following steps:
1. Create a branch at `veloren/veloren` repo, named `<User>/update-toolchain`
    1. Adjust `rust-toolchain`
    2. Make adjustments to address any new clippy warnings, rustfmt changes, and make to sure it still builds. You can find the exact commands to check here:
       https://gitlab.com/veloren/veloren/-/blob/master/.gitlab/CI/check.gitlab-ci.yml
       https://gitlab.com/veloren/veloren/-/blob/master/.gitlab/CI/build.gitlab-ci.yml
2. Create a branch at `veloren/veloren-docker-ci` repo, named `<User>/update-toolchain`
    1. Adjust `RUST_TOOLCHAIN` in `base/common.Dockerfile`, push and create a MR.
    2. Run a manual pipeline via https://gitlab.com/veloren/veloren-docker-ci/-/pipelines on `<User>/update-toolchain`, you will be asked for a variable `BRANCH` which refers to the branch on `veloren/veloren`, choose `<User>/update-toolchain`.
    3. After the pipeline completed check the registry for your new image tag, it should match the git hash of your lastest commit.
    4. Merge that MR to `veloren/veloren-docker-ci`.
3. Create a new commit at `veloren/veloren` repo. Adjust `CACHE_IMAGE_TAG` in file: `.gitlab-ci.yml`. Create a MR, wait for pipeline to succeed and merge it.
4. Adjust the `CACHE_IMAGE_TAG` and `rust-toolchain` on the following repos too:
```
veloren/torvus
veloren/auth
veloren/airshipper
veloren/labbot
veloren/serverbrowser
```
List of repos that just depend on `latest` tag and usually not need an adjustment
```
xMAC94x/treestand
xMAC94x/prometheus-hyper
xMAC94x/threads
xMAC94x/tlid
```
