# Linux release cache

# Mandatory arg
ARG FROM_TAG=please-specify-a-tag-also-hopefully-this-tag-wont-be-used
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/linux-x86_64:$FROM_TAG

# Allow using an alternative branch so toolchain updates can be prepared before altering master
ARG BRANCH=master

RUN mkdir /dockercache && cd /dockercache \
    && . /root/.cargo/env \
    # Install cargo-bundle, its currently evaluated for Airshipper
    && time cargo install --git https://github.com/burtonageo/cargo-bundle --rev 8e0db03a49310991fb29a5d3833ea638d6bff5b4 cargo-bundle \
    && rm -r $HOME/.cargo/registry/cache $HOME/.cargo/registry/src \
    # Clone the repo
    && time git clone --depth 10 "https://gitlab.com/veloren/${PROJECTNAME}.git" --branch "${BRANCH}" \
    && cd veloren \
    # Create target folder
    && mkdir /dockercache/target \
    && ln -s /dockercache/target target \
    # Print the current branch and commit
    && echo ${BRANCH} && echo $(git rev-parse --short HEAD) \
    # Get lfs files
    && time git lfs install && time git lfs pull \
    # Set the toolchain
    && echo ${RUST_TOOLCHAIN} > ./rust-toolchain \
    # NOTE: we duplicate the cloning section above in all the cache Dockerfiles so that we can delete the
    # cloned repo within the same RUN command to save space (i.e. keep it from being part of a layer)
    # kaniko causes some sort of issue with cargo's caching when trying to use multistage builds
    # (i.e. `COPY --from`) to circumvent this issue in a more elegant manner
    && . ./.gitlab/scripts/env.sh \
    # Build to fill cache
    && . ./.gitlab/scripts/linux-x86_64.sh \
    # Remove everything except the target folder
    && cd .. && rm -r veloren
