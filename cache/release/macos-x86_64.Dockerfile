# Mandatory arg
ARG FROM_TAG=please-specify-a-tag-also-hopefully-this-tag-wont-be-used
FROM registry.gitlab.com/veloren/veloren-docker-ci/base/macos-x86_64:$FROM_TAG

# Allow using an alternative branch so toolchain updates can be prepared before altering master
ARG BRANCH=master

RUN mkdir /dockercache && cd /dockercache \
    && . /root/.cargo/env \
    # Install cargo-bundle, its currently evaluated for Airshipper
    && time cargo install --git https://github.com/burtonageo/cargo-bundle --rev 8e0db03a49310991fb29a5d3833ea638d6bff5b4 cargo-bundle \
    && rm -r $HOME/.cargo/registry/cache $HOME/.cargo/registry/src \
    # Clone the repo
    && time git clone --depth 10 "https://gitlab.com/veloren/${PROJECTNAME}.git" --branch "${BRANCH}" \
    && cd veloren \
    # Create target folder
    && mkdir /dockercache/target \
    && ln -s /dockercache/target target \
    # Print the current branch and commit
    && echo ${BRANCH} && echo $(git rev-parse --short HEAD) \
    # Get lfs files
    && time git lfs install && time git lfs pull \
    # Set the toolchain
    && echo ${RUST_TOOLCHAIN} > ./rust-toolchain \
    # NOTE: we duplicate the cloning section above in all the cache Dockerfiles so that we can delete the
    # cloned repo within the same RUN command to save space (i.e. keep it from being part of a layer)
    # kaniko causes some sort of issue with cargo's caching when trying to use multistage builds
    # (i.e. `COPY --from`) to circumvent this issue in a more elegant manner
    && . ./.gitlab/scripts/env.sh \
    # Build to fill cache
    && VELOREN_ASSETS=assets WINIT_LINK_COLORSYNC=true PATH="/osxcross/target/bin:$PATH" COREAUDIO_SDK_PATH=/osxcross/target/SDK/MacOSX10.13.sdk CC=o64-clang CXX=o64-clang++ time cargo build --target=x86_64-apple-darwin --release \
    # Remove everything except the target folder
    && cd .. && rm -r veloren

#TODO: refetched index? was this tested since adding the index copy


#NOTE: shaderc linking fails with shaderc from vulkan SDK
# error: linking with `/osxcross/target/bin/x86_64-apple-darwin17-clang` failed: exit code: 1
#   = note: ld: warning: building for macOS 10.7.0 is deprecated
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(shaderc.cc.o)) was built for newer macOS version (10.15) than being linked (10.7)
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(spirv_tools_wrapper.cc.o)) was built for newer macOS version (10.15) than being linked (10.7)
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(version_profile.cc.o)) was built for newer macOS version (10.15) than being linked (10.7)
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(resources.cc.o)) was built for newer macOS version (10.15) than being linked (10.7)
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(compiler.cc.o)) was built for newer macOS version (10.15) than being linked (10.7)
# 
#           ( etc...)
# 
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(validate_scopes.cpp.o)) was built for newer macOS version (10.15) than being linked (10.7)
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(validate_memory_semantics.cpp.o)) was built for newer macOS version (10.15) than being linked (10.7)
#           ld: warning: object file (/tmp/rustcHF19Ew/libshaderc_sys-4b53f8778cbf18f7.rlib(construct.cpp.o)) was built for newer macOS version (10.15) than being linked (10.7)
#         
#   Undefined symbols for architecture x86_64:
#             "____chkstk_darwin", referenced from:
#                 (anonymous namespace)::Parser::parseOperand(unsigned long, spv_parsed_instruction_t*, spv_operand_type_t, std::__1::vector<unsigned int, std::__1::allocator<unsigned int> >*, std::__1::vector<spv_parsed_operand_t, std::__1::allocator<spv_parsed_operand_t> >*, std::__1::vector<spv_operand_type_t, std::__1::allocator<spv_operand_type_t> >*) in libshaderc_sys-4b53f8778cbf18f7.rlib(binary.cpp.o)
#                 glslang::TParseVersions::initializeExtensionBehavior() in libshaderc_sys-4b53f8778cbf18f7.rlib(Versions.cpp.o)
#                 glslang::HlslParseContext::decomposeSampleMethods(glslang::TSourceLoc const&, glslang::TIntermTyped*&, TIntermNode*) in libshaderc_sys-4b53f8778cbf18f7.rlib(hlslParseHelper.cpp.o)
#                 glslang::HlslParseContext::decomposeIntrinsic(glslang::TSourceLoc const&, glslang::TIntermTyped*&, TIntermNode*) in libshaderc_sys-4b53f8778cbf18f7.rlib(hlslParseHelper.cpp.o)
#                 yyparse(glslang::TParseContext*) in libshaderc_sys-4b53f8778cbf18f7.rlib(glslang_tab.cpp.o)
#                 glslang::TIntermConstantUnion::fold(glslang::TOperator, glslang::TType const&) const in libshaderc_sys-4b53f8778cbf18f7.rlib(Constant.cpp.o)
#                 spvtools::val::(anonymous namespace)::ValidateBinaryUsingContextAndValidationState(spv_context_t const&, unsigned int const*, unsigned long, spv_diagnostic_t**, spvtools::val::ValidationState_t*) in libshaderc_sys-4b53f8778cbf18f7.rlib(validate.cpp.o)
#                 ...
#           ld: symbol(s) not found for architecture x86_64
