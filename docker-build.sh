#!/bin/bash

# Make sure to commit before running
git diff-index -p --quiet HEAD --exit-code || (echo "Make sure to commit any changes before running!!! (check 'git status')" & exit 1);
# Get short version of commit hash
commit=$(git rev-parse --short=8 HEAD);
if [[ ${#commit} < 7 ]]; then
    echo "failed to get commit hash, got this: $commit";
    exit 1;
fi

branch="master"

registry="registry.gitlab.com/veloren/veloren-docker-ci";
base_name="$registry/base";
cache_name="$registry/cache";
fetch_name="$registry/fetch";

# Make sure we have access to docker, if not set correct rights or use root user
docker login $registry;

# STAGE: base-start (grouped by ci build stages for organization)
# Build and push base/common image
time docker build -t $base_name/common:$commit --no-cache -f ./base/common.Dockerfile .;
docker push $base_name/common:$commit;
# # Build and push base/osxcross-x86_64 image
# time docker build -t $base_name/osxcross-x86_64:$commit --no-cache -f ./base/osxcross.Dockerfile .;
# docker push $base_name/osxcross-x86_64:$commit;
# Build and push base/shaderc-linux-x86_64 image
time docker build -t $base_name/shaderc-linux-x86_64:$commit --no-cache -f ./base/shaderc/linux.Dockerfile .;
docker push $base_name/shaderc-linux-x86_64:$commit;
# # Build and push base/shaderc-macos-x86_64 image
# time docker build -t $base_name/shaderc-macos-x86_64:$commit --no-cache -f ./base/shaderc/macos.Dockerfile .;
# docker push $base_name/shaderc-macos-x86_64:$commit;

# STAGE: base-platform
# Build and push base/linux-x86_64 image
time docker build -t $base_name/linux-x86_64:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./base/linux-x86_64.Dockerfile .;
docker push $base_name/linux-x86_64:$commit;
# Build and push base/linux-aarch64 image
time docker build -t $base_name/linux-aarch64:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./base/linux-aarch64.Dockerfile .;
docker push $base_name/linux-aarch64:$commit;
# Build and push base/windows-x86_64 image
time docker build -t $base_name/windows-x86_64:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./base/windows-x86_64.Dockerfile .;
docker push $base_name/windows-x86_64:$commit;
# # Build and push base/macos-x86_64 image
# time docker build -t $base_name/macos:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./base/macos-x86_64.Dockerfile .;
# docker push $base_name/macos-x86_64:$commit;

# STAGE: cache
# Build and push cache/bench image
time docker build -t $cache_name/bench:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./cache/bench.Dockerfile .;
docker push $cache_name/bench:$commit;
docker tag $cache_name/bench:$commit $cache_name/bench:latest;
docker push $cache_name/bench:latest;
# Build and push cache/quality image
time docker build -t $cache_name/quality:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./cache/quality.Dockerfile .;
docker push $cache_name/quality:$commit;
docker tag $cache_name/quality:$commit $cache_name/quality:latest;
docker push $cache_name/quality:latest;
# Build and push cache/tarpaulin image
time docker build -t $cache_name/tarpaulin:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./cache/tarpaulin.Dockerfile .;
docker push $cache_name/tarpaulin:$commit;
docker tag $cache_name/tarpaulin:$commit $cache_name/tarpaulin:latest;
docker push $cache_name/tarpaulin:latest;
# Build and push cache/release-linux-x86_64 image
time docker build -t $cache_name/release-linux-x86_64:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./cache/release/linux-x86_64.Dockerfile .;
docker push $cache_name/release-linux-x86_64:$commit;
docker tag $cache_name/release-linux-x86_64:$commit $cache_name/release-linux-x86_64:latest;
docker push $cache_name/release-linux-x86_64:latest;
# Build and push cache/release-linux-aarch64 image
time docker build -t $cache_name/release-linux-aarch64:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./cache/release/linux-aarch64.Dockerfile .;
docker push $cache_name/release-linux-aarch64:$commit;
docker tag $cache_name/release-linux-aarch64:$commit $cache_name/release-linux-aarch64:latest;
docker push $cache_name/release-linux-aarch64:latest;
# Build and push cache/release-windows-x86_64 image
time docker build -t $cache_name/release-windows-x86_64:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch -f ./cache/release/windows-x86_64.Dockerfile .;
docker push $cache_name/release-windows-x86_64:$commit;
docker tag $cache_name/release-windows-x86_64:$commit $cache_name/release-windows-x86_64:latest;
docker push $cache_name/release-windows-x86_64:latest;
# # Build and push cache/release-macos image
# time docker build -t $cache_name/release-macos:$commit --no-cache --build-arg FROM_TAG=$commit --build-arg BRANCH=$branch ./cache/release/macos;
# docker push $cache_name/release-macos:$commit;

