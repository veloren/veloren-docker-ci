#!/bin/bash

# Check that the working dir is the runner dir
# Note: this is for the path to the template file to be accurate
# Note: this is also for the path to the setup-docuum.sh script to be accurate
pwdpath=$(pwd)
if [[ $(basename $pwdpath) != "runner" ]]; then
  echo "you must cd into the ./runner/ directory before executing this script"
  exit 1;
fi

# Read the gitlab token and runner name from the user
echo "Please enter Veloren's Gitlab runner token. It can be found at https://gitlab.com/veloren/veloren/settings/ci_cd"
read -p "Token: " GITLAB_RUNNER_TOKEN

echo "\nWhat should this runner be called?"
read -p "Name: " GITLAB_RUNNER_NAME

# Setup docuum to auto clean old images when the runner is out of space
./setup-docuum.sh

# Start the gitlab runner container
docker run -d --security-opt seccomp=unconfined --name "$GITLAB_RUNNER_NAME" --restart always \
  -v /srv/"$GITLAB_RUNNER_NAME"/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

# Check for an error from starting the runner
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "There was a error setting up the gitlab-runner docker container, exiting"
  exit 2;
fi

# Register the runner with gitlab
echo "Registering this specific runner with gitlab"

docker run --rm -it \
  -v /srv/"$GITLAB_RUNNER_NAME"/config:/etc/gitlab-runner \
  -v "$(pwd)"/template.toml:/srv/template.toml \
  gitlab/gitlab-runner \
    register \
      --non-interactive \
      --executor "docker" \
      --docker-image ubuntu:22.04 \
      --url "https://gitlab.com/" \
      --registration-token "$GITLAB_RUNNER_TOKEN" \
      --description "$GITLAB_RUNNER_NAME" \
      --tag-list "build, check, veloren/veloren, veloren/*" \
      --run-untagged="true" \
      --locked="false" \
      --template-config /srv/template.toml

# NOTES:
# Default docker image, unused since we specify them in our ymls
#   --docker-image ubunut:18.04
# Gitlab host url
#   --url "https://gitlab.com/"
# Runs jobs with these tags
#   --tag-list "build, check, veloren/veloren, veloren/*"
# Runs untagged jobs in addition to those in the tag-list
#   --run-untagged="true"
# Runner not locked to the current project
#   --locked="false"
